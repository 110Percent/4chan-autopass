# 4chan Auto-pass

Quick hack to automatically authenticate 4chan pass when you open a board unauthenticated.  
  
### [Click me](https://gitlab.com/110Percent/4chan-autopass/-/raw/master/autopass.user.js)  

Set "token" and "pin" in the script's values in your userscript manager.