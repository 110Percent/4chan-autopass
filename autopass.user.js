// ==UserScript==
// @name        Auto-Authenticate 4chan Pass
// @namespace   Violentmonkey Scripts
// @match       https://boards.4channel.org/*
// @match       https://sys.4channel.org/*
// @match       https://boards.4chan.org/*
// @match       https://sys.4chan.org/*
// @grant       GM.setValue
// @grant       GM.getValue
// @version     1.0
// @author      110Percent
// @description Set "token" and "pin" in the script's values.
// ==/UserScript==

if (window.location.hostname.startsWith("boards.")) {
  const match = document.cookie.match(new RegExp("(^| )pass_enabled=([^;]+)"));
  if (match && match[2] === "1") {
    console.log("4chan pass already authenticated!");
    return;
  }
  console.log("Auth 4chan Pass");
  const domain = window.location.hostname
    .split(".")
    .slice(window.location.hostname.split(".").length - 2)
    .join(".");

  window.open(`https://sys.${domain}/auth`, "_blank");
} else if (
  window.location.hostname.startsWith("sys.") &&
  document.getElementById("auth-btn")
) {
  (async function () {
    const creds = {
      token: await GM.getValue("token", null),
      pin: await GM.getValue("pin", null),
    };

    console.log(creds);

    if (!creds.token || !creds.pin) {
      console.log("No 4chan pass credentials provided!");
      return;
    }

    document.getElementById("field-id").setAttribute("value", creds.token);
    document.getElementById("field-pin").setAttribute("value", creds.pin);
    document.getElementById("auth-btn").click();
  })();
}
